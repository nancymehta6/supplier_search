﻿using System;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;
using System.Web.Configuration;

namespace SupplierSearch.Models
{
    public class Suppliers
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Town { get; set; }
        public string Postcode { get; set; }
        public string Title { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string State { get; set; }
        public string Phone { get; set; }
        public string Contact { get; set; }
        public string Email { get; set; }
    }
    public class SuppliersDbContext : DbContext
    {
        public DbSet<Suppliers> Suppliers { get; set; }
    }
}
﻿using System.Collections.Generic;
using System.Web.Mvc;
using System;
using Excel = Microsoft.Office.Interop.Excel;
using System.IO;
using Accord.IO;
using System.Data;
using SupplierSearch.Models;
using System.Linq;
using System.Collections;
using Spire.Pdf.Exporting.XPS.Schema;

namespace SupplierSearch.Controllers
{
    public class HomeController : Controller
    {
        private SuppliersDbContext db = new SuppliersDbContext();
        public ActionResult Index()
        {

            if (!(db.Suppliers.Count<Suppliers>() > 0))
            {

                Excel.Application xlApp = new Excel.Application();
                Excel.Workbook xlWorkbook = xlApp.Workbooks.Open(@"C:\Users\Nancy Mehta\Documents\Visual Studio 2017\Projects\SupplierSearch\data\Suppliers.csv");
                Excel._Worksheet xlWorksheet = xlWorkbook.Sheets[1];
                Excel.Range xlRange = xlWorksheet.UsedRange;
                //Array myvalues = (Array)(xlRange.Columns[3].Cells.Value2.ToString);

                int rowCount = xlRange.Rows.Count;
                int colCount = xlRange.Columns.Count;
                //string Value1 = postCode;

                for (int i = 1; i < rowCount; i++)

                {

                    Suppliers entry_data = new Suppliers()
                    {
                        ID = i,
                        Name = Convert.ToString(xlRange.Cells[i, 1].Value2),
                        Town = Convert.ToString(xlRange.Cells[i, 2].Value2),
                        Postcode = Convert.ToString(xlRange.Cells[i, 3].Value2),
                        Title = Convert.ToString(xlRange.Cells[i, 4].Value2),
                        Address1 = Convert.ToString(xlRange.Cells[i, 5].Value2),
                        Address2 = Convert.ToString(xlRange.Cells[i, 6].Value2),
                        State = Convert.ToString(xlRange.Cells[i, 7].Value2),
                        Phone = Convert.ToString(xlRange.Cells[i, 8].Value2),
                        Contact = Convert.ToString(xlRange.Cells[i, 11].Value2),
                        Email = Convert.ToString(xlRange.Cells[i, 12].Value2)
                    };
                    db.Suppliers.Add(entry_data);
                    db.SaveChanges();

                }
            }

            return View();

        }

        [HttpPost]
        public ActionResult Index(string postCode)
        {

            string[] nearest = new string[5];
            var GenreQry = from s in db.Suppliers where s.Postcode == postCode
                           select s;
            if (GenreQry.Count() != 0)
            {
                foreach (Suppliers sup in GenreQry)
                {
                    nearest[0] = sup.Name;
                    nearest[1] = sup.Town;
                    ViewBag.Message = "Exact Match";
                    ViewBag.nearest = nearest;
                } 
            } else
            {
                SearchNearest(postCode);
            }
            
            
            return View("displayData");
        }
        public ActionResult SearchNearest(string postCode)
        {
            int[] ids = new int[5];
            string[,] nearest = new string[5,8];

            var GenreQry = (from s in db.Suppliers select s).ToList();

            for (int i = 0; i < 5; i++)
            {
               
                int smallestDiff = Math.Abs(Int32.Parse(postCode) - Int32.Parse((GenreQry.ElementAt(2).Postcode).ToString()));
                int smallestDiffIndex = 1;
                int currDiff = smallestDiff;
                for (int j = 2; j < GenreQry.Count(); j++)
                {
                    
                        if ((GenreQry.ElementAt(j).Postcode).ToString() != "" &&!(ids.Contains(j))){
                            currDiff = Math.Abs(Int32.Parse(postCode) - Int32.Parse((GenreQry.ElementAt(j).Postcode).ToString()));
                        
                        if (currDiff < smallestDiff)
                            {
                                smallestDiff = currDiff;
                                smallestDiffIndex = j;
                            }
                        }
                   
                }
                ids[i] = smallestDiffIndex;
                nearest[i,0] = GenreQry.ElementAt(smallestDiffIndex).Name.ToString();
                nearest[i,1] = GenreQry.ElementAt(smallestDiffIndex).Address1.ToString() ; 
                nearest[i, 2] = GenreQry.ElementAt(smallestDiffIndex).Town.ToString();
                nearest[i, 3] = GenreQry.ElementAt(smallestDiffIndex).State.ToString();
                nearest[i, 4] = GenreQry.ElementAt(smallestDiffIndex).Postcode.ToString();
                nearest[i,5] = GenreQry.ElementAt(smallestDiffIndex).Phone.ToString();
                if(GenreQry.ElementAt(smallestDiffIndex).Email != null)
                    nearest[i,6] = GenreQry.ElementAt(smallestDiffIndex).Email.ToString();
                if (GenreQry.ElementAt(smallestDiffIndex).Contact != null)
                    nearest[i,7] = GenreQry.ElementAt(smallestDiffIndex).Contact.ToString();
                GenreQry.RemoveAt(smallestDiffIndex);
            }
            ViewBag.nearest = nearest;
            ViewBag.Message = "Nearest Result";
            return View(("displayData"));
        }

    }
}

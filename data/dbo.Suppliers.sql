﻿CREATE TABLE [dbo].[Suppliers] (
    [ID]       INT            IDENTITY (1, 1) NOT NULL,
    [Name]     NVARCHAR (MAX) NULL,
    [Town]     NVARCHAR (MAX) NULL,
    [Postcode] NVARCHAR (MAX) NULL,
    [Title]    NVARCHAR (MAX) NULL,
    [Address1] NVARCHAR (MAX) NULL,
    [Address2] NVARCHAR (MAX) NULL,
    [State]    NVARCHAR (MAX) NULL,
    [Phone]    NVARCHAR (MAX) NULL,
    [Contact]  NVARCHAR (MAX) NULL,
    [Email]    NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_dbo.Suppliers] PRIMARY KEY CLUSTERED ([ID] ASC)
);

